const path = require('path');
const fs = require('fs');
const replace = require("replace");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const WebpackOnBuildPlugin = require('on-build-webpack');

const stylusLoader = ExtractTextPlugin.extract({
    fallback: "style-loader",
    use: "css-loader!stylus-loader",
});

const PRODUCTION = process.env.NODE_ENV === 'production';

const entry = PRODUCTION
    ? [ './src/index.js' ]
    : [
        './src/index.js',
        'webpack/hot/dev-server',
        'webpack-dev-server/client?http://localhost:8080'
    ];

const plugins = PRODUCTION
    ? [
        new webpack.optimize.UglifyJsPlugin({
            comments: false,
            mangle: false,
            compress: {
                warnings: true
            }
        }),
        new HTMLWebpackPlugin({
            template: './index-template.html'
        })
      ]
    : [ new webpack.HotModuleReplacementPlugin() ];

plugins.push(
    new webpack.DefinePlugin({
        PRODUCTION: PRODUCTION,
        DEVELOPMENT: !PRODUCTION
    }),
    new ExtractTextPlugin("main.min.css")
);

if (PRODUCTION) {

    plugins.push(
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: {removeAll: true } },
            canPrint: true
        })
    );

    plugins.push(
        new CopyWebpackPlugin([
            {from: './fonts/*'},
            {from: './images/*'}
        ])
    );

    plugins.push(
        new WebpackOnBuildPlugin(function(stats) {
            console.log('finished build');
        })
    );
}

const loaders = [
    {
        test: /\.js$/,
        loaders: [ 'babel-loader' ],
        exclude: '/node_modules/'
    },
    {
        test: /\.styl$/,
        loader: PRODUCTION ? stylusLoader : [ 'style-loader', 'css-loader', 'stylus-loader' ]
    },
    {
        test: /\.css$/,
        loaders: [ 'style-loader', 'css-loader' ],
        exclude: '/node_modules/'
    }
];


module.exports = {
    devtool: 'source-map',
    entry: entry,
    plugins: plugins,
    module: {
        unknownContextRegExp: /$^/,
        unknownContextCritical: false,
        loaders: loaders
    },
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: PRODUCTION ? '' : '/dist/',
        filename: PRODUCTION ? 'bundle.min.js' : 'bundle.js'
    },
    externals: {
        
    }
};